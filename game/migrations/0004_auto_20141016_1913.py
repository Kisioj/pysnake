# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0003_auto_20141006_2324'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamehistory',
            name='error',
            field=models.CharField(max_length=512, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='gamehistory',
            name='moves_history',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='gamehistory',
            name='snake1_result',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='gamehistory',
            name='snake2_result',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
