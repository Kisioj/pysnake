# from mysnake import MySnake as Snake
from collections import namedtuple
from random import randrange, choice
from copy import copy
from itertools import product
import json
import signal

BOARD_WIDTH = 30
BOARD_HEIGHT = 30
STARTING_POINT_AREA_SIZE = 6

WINNING_POINTS = 10
TIE_POINTS = 5

Point = namedtuple('Point', 'x y')

def enum(**enums):
    return type('Enum', (), enums)

Outcomes = enum(
	go_on=0,
	snake1=1,
	snake2=2,
	tie=3
)

class Snake(object):
	head = None
	body = None
	has_eaten = False
	board_width = None
	board_height = None
	name = None
	data = None

	def __init__(self, name, initial_point):
		self.name = name
		self.head = initial_point
		self.body = [self.head]

	def __unicode__(self):
		return self.name or 'snake'

def executable(code):
	safe_list = ['math','acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'cosh', 'de grees', 'e', 'exp', 'fabs', 'floor', 'fmod', 'frexp', 'hypot', 'ldexp', 'log', 'log10', 'modf', 'pi', 'pow', 'radians', 'random', 'sin', 'sinh', 'sqrt', 'tan', 'tanh'] 
	safe_dict = dict([(k, locals().get(k, None)) for k in safe_list])
	safe_dict['abs'] = abs

	safe_globals_list = ['__import__', 'all', 'isinstance', 'dict', 'sorted', 'True', 'False', 'list', 'iter', 'set', 'reduce', 'slice', 'sum', 'abs', 'exit', 'None', 'len', 'filter', 'range', 'pow', 'float', 'enumerate', 'long', 'xrange', 'type', 'tuple', 'str', 'property', 'int', 'min', 'any', 'bool', 'map', 'max', 'object', '__name__']
	safe_globals_dict = dict([(k, globals().get('__builtins__', {}).get(k)) for k in safe_globals_list])
	exec(code, {"__builtins__": safe_globals_dict}, safe_dict)
	return safe_dict['move']

class Board(object):
	board_changes = None
	free_points = None

	def __init__(self, snake1_move, snake2_move, snake1_executable=False, snake2_executable=False):
		self.free_points = map(lambda (x, y): Point(x, y), product(range(0, BOARD_WIDTH), range(0, BOARD_HEIGHT)))
		self.snake1 = Snake(
			'snake1',
			Point(
				x=randrange(0, STARTING_POINT_AREA_SIZE),
				y=randrange(0, STARTING_POINT_AREA_SIZE)
			)
		)
		self.snake2 = Snake(
			'snake2',
			Point(
				x=randrange(BOARD_WIDTH-STARTING_POINT_AREA_SIZE, BOARD_WIDTH),
				y=randrange(BOARD_HEIGHT-STARTING_POINT_AREA_SIZE, BOARD_HEIGHT)
			)
		)
		self.food = Point(
			x=randrange((BOARD_WIDTH-STARTING_POINT_AREA_SIZE)/2, (BOARD_WIDTH+STARTING_POINT_AREA_SIZE)/2),
			y=randrange((BOARD_HEIGHT-STARTING_POINT_AREA_SIZE)/2, (BOARD_HEIGHT+STARTING_POINT_AREA_SIZE)/2)
		)
		self.free_points.remove(self.snake1.head)
		self.free_points.remove(self.snake2.head)
		self.free_points.remove(self.food)
		if snake1_executable:
			self.snake1_move = executable(snake1_move)
		else:
			self.snake1_move = snake1_move
		if snake2_executable:
			self.snake2_move = executable(snake2_move)
		else:
			self.snake2_move = snake2_move

	def generate_board_changes(self):
		self.board_changes = {
			'snake1': {},
			'snake2': {},
			'food': {}
		}

	def get_random_free_point(self):
		random_point = choice(self.free_points)
		self.free_points.remove(random_point)
		return random_point

	def simple_point(self, point):
		return "(%s,%s)" %(point.x, point.y)

	def move_snake(self, snake, snake_move, enemy_snake):
		def sigh(signum, frame):
			raise RuntimeError("Timeout!")
		signal.signal(signal.SIGALRM, sigh)
		signal.alarm(10)
		response = snake_move(snake1=copy(snake), snake2=copy(enemy_snake), food=copy(self.food), data=snake.data, board_width=BOARD_WIDTH, board_height=BOARD_HEIGHT)
		signal.alarm(0)
		if isinstance(response, tuple):
			direction, snake.data = response
		else:
			direction = response
		if direction == 'l':
			snake.head = Point(x=snake.head.x-1, y=snake.head.y)
		elif direction == 'u':
			snake.head = Point(x=snake.head.x, y=snake.head.y-1)
		elif direction == 'r':
			snake.head = Point(x=snake.head.x+1, y=snake.head.y)
		elif direction == 'd':
			snake.head = Point(x=snake.head.x, y=snake.head.y+1)
		else:
			raise Exception('Snake returned %s instead one of l u r or d' %str(direction))
		snake.body.insert(0, snake.head)
		self.board_changes[snake.name]['a'] = self.simple_point(snake.head)
		try:
			self.free_points.remove(snake.head)
		except ValueError:
			pass # two snakes may be here one after other
		if snake.head == self.food:
			self.board_changes['food']['r'] = self.simple_point(self.food)
			self.food = self.get_random_free_point()
			self.board_changes['food']['a'] = self.simple_point(self.food)
		else:
			self.board_changes[snake.name]['r'] = self.simple_point(snake.body[-1])
			self.free_points.append(snake.body[-1])
			del snake.body[-1]

	def outside_board(self, snake):
		if 0 <= snake.head.x <= BOARD_WIDTH-1 and 0 <= snake.head.y <= BOARD_HEIGHT-1:
			return False
		return True

	def tick(self):
		self.move_snake(self.snake1, self.snake1_move, self.snake2)
		self.move_snake(self.snake2, self.snake2_move, self.snake1)

		snake1_winning = False
		snake2_winning = False
		if self.snake2.head in self.snake1.body or self.snake2.head in self.snake2.body[1:] or self.outside_board(self.snake2):
			snake1_winning = True
		if self.snake1.head in self.snake1.body[1:] or self.snake1.head in self.snake2.body or self.outside_board(self.snake1):
			snake2_winning = True

		if snake1_winning and snake2_winning:
			return Outcomes.tie
		elif snake1_winning:
			return Outcomes.snake1
		elif snake2_winning:
			return Outcomes.snake2
		else:
			return Outcomes.go_on

	def turn(self):
		error = None
		self.generate_board_changes()
		self.board_changes[self.snake1.name]['a'] = self.simple_point(self.snake1.head)
		self.board_changes[self.snake2.name]['a'] = self.simple_point(self.snake2.head)
		self.board_changes['food']['a'] = self.simple_point(self.food)
		yield self.board_changes
		i = 0
		while True:
			i += 1
			if i >= 5000:
				break
			self.generate_board_changes()
			try:
				result = self.tick()
			except Exception as e:
				error = e
				break
			if result == Outcomes.go_on:
				yield self.board_changes
			else:
				break
		if error:
			yield error
		else:
			snake1_result = len(self.snake1.body)
			snake2_result = len(self.snake2.body)
			if result == Outcomes.snake1:
				snake1_result += WINNING_POINTS
			elif result == Outcomes.snake2:
				snake2_result += WINNING_POINTS
			elif result == Outcomes.tie:
				snake1_result += TIE_POINTS
				snake2_result += TIE_POINTS
			self.board_changes.update({
				'result': {
					'snake1': snake1_result,
					'snake2': snake2_result
				}
			})
			yield self.board_changes
